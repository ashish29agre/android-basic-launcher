package in.ashish29agre.basiclauncher;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class DrawerClickListener implements OnItemClickListener{
	private Context mContext;
	private PackageManager pm;
	private BasicActivity.Pac pacs[];
	public DrawerClickListener (Context context, BasicActivity.Pac[] pacs, PackageManager pm) {
		this.mContext = context;
		this.pacs = pacs;
		this.pm = pm;
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		Intent launchIntent = pm.getLaunchIntentForPackage(pacs[position].name);
		mContext.startActivity(launchIntent);
	}

}
