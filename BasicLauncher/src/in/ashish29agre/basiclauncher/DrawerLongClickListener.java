package in.ashish29agre.basiclauncher;

import android.content.Context;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SlidingDrawer;
import android.widget.TextView;

public class DrawerLongClickListener implements OnItemLongClickListener {
	private Context mContext;
	private PackageManager pm;
	private BasicActivity.Pac pacs[];
	private SlidingDrawer slidingDrawer;
	private RelativeLayout homeView;
	private LayoutInflater inflater;

	public DrawerLongClickListener(Context context,
			PackageManager pm, SlidingDrawer slidingDrawer,
			RelativeLayout homeView) {
		this.mContext = context;
		this.pm = pm;
		this.slidingDrawer = slidingDrawer;
		this.homeView = homeView;
		inflater = (LayoutInflater) this.mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View item, int arg2,
			long arg3) {
		LayoutParams lp = new LayoutParams(item.getWidth(), item.getHeight());
		lp.leftMargin = (int) item.getX();
		lp.topMargin = (int) item.getX();
		LinearLayout ll  = (LinearLayout) inflater.inflate(R.layout.drawer_item, null);
		((ImageView)ll.findViewById(R.id.icon)).setImageDrawable(((ImageView)item.findViewById(R.id.icon)).getDrawable());
		((TextView)ll.findViewById(R.id.icon_text)).setText(((TextView)item.findViewById(R.id.icon)).getText());
		homeView.addView(ll, lp);
		slidingDrawer.animateClose();
		slidingDrawer.bringToFront();
		return false;
	}

}
