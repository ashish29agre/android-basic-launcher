package in.ashish29agre.basiclauncher;

import java.util.List;

import android.R.id;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;

public class BasicActivity extends Activity {

	Pac[] pacs;
	PackageManager pm;
	GridView drawerGrid;
	SlidingDrawer slidingDrawer;
	RelativeLayout homeView;
	DrawerAdapter drawerAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_basic);
		pm = getPackageManager();
		drawerGrid = (GridView) findViewById(R.id.content);
		slidingDrawer = (SlidingDrawer) findViewById(R.id.drawer);
		homeView = (RelativeLayout) findViewById(R.id.home_view);
		setPacs();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_PACKAGE_ADDED);
		filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
		filter.addDataScheme("package");
		registerReceiver(new PacReceiver(), filter);
	}
	
	public void setPacs() {
		final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		List<ResolveInfo> pacsList = pm.queryIntentActivities(mainIntent, 0);
		pacs = new Pac[pacsList.size()];
		for(int i=0;i<pacsList.size();i++) {
			pacs[i] = new Pac();
			pacs[i].icon = pacsList.get(i).loadIcon(pm);
			pacs[i].name = pacsList.get(i).activityInfo.packageName;
			pacs[i].label = pacsList.get(i).loadLabel(pm).toString();
		}
		drawerAdapter = new DrawerAdapter(this, pacs);
		drawerGrid.setAdapter(drawerAdapter);
		drawerGrid.setOnItemClickListener(new DrawerClickListener(this, pacs, pm));
		drawerGrid.setOnItemLongClickListener(new DrawerLongClickListener(this, pm, slidingDrawer, homeView));
	}
	

	class Pac {
		Drawable icon;
		String name;
		String label;
	}
	
	class PacReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			setPacs();
		}
		
	}

}
