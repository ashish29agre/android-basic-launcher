package in.ashish29agre.basiclauncher;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerAdapter extends BaseAdapter {
	private Context mContext;
	private BasicActivity.Pac pacs[];
	private LayoutInflater inflater;

	public DrawerAdapter(Context context, BasicActivity.Pac pacs[]) {
		this.mContext = context;
		this.pacs = pacs;
		inflater = (LayoutInflater) this.mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return pacs.length;
	}

	@Override
	public Object getItem(int pos) {
		return pacs[pos];
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int pos, View view, ViewGroup viewGroup) {
		View rowView = view;
		ViewHolder holder;
		if (rowView == null) {
			rowView = inflater.inflate(R.layout.drawer_item, null);
			holder = new ViewHolder();
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		holder.icon = (ImageView) rowView.findViewById(R.id.icon);
		holder.iconText = (TextView) rowView.findViewById(R.id.icon_text);
		holder.icon.setImageDrawable(pacs[pos].icon);
		holder.iconText.setText(pacs[pos].label);
		return rowView;
	}

	public static class ViewHolder {
		ImageView icon;
		TextView iconText;
	}
	

}
